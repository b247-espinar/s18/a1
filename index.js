/*
	
	1.  Create a function which will be able to add two numbers.
		-Numbers must be provided as arguments.
		-Display the result of the addition in our console.
		-function should only display result. It should not return anything.

		Create a function which will be able to subtract two numbers.
		-Numbers must be provided as arguments.
		-Display the result of subtraction in our console.
		-function should only display result. It should not return anything.

		-invoke and pass 2 arguments to the addition function
		-invoke and pass 2 arguments to the subtraction function

	2.  Create a function which will be able to multiply two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the multiplication.

		Create a function which will be able to divide two numbers.
			-Numbers must be provided as arguments.
			-Return the result of the division.

	 	Create a global variable called outside of the function called product.
			-This product variable should be able to receive and store the result of multiplication function.
		Create a global variable called outside of the function called quotient.
			-This quotient variable should be able to receive and store the result of division function.

		Log the value of product variable in the console.
		Log the value of quotient variable in the console.

	3. 	Create a function which will be able to get total area of a circle from a provided 		radius.
			-a number should be provided as an argument.
			-look up the formula for calculating the area of a circle with a provided/given radius.
			-look up the use of the exponent operator.
			-you can save the value of the calculation in a variable.
			-return the result of the area calculation.

		Create a global variable called outside of the function called circleArea.
			-This variable should be able to receive and store the result of the circle area calculation.

	Log the value of the circleArea variable in the console.

	4. 	Create a function which will be able to get total average of four numbers.
			-4 numbers should be provided as an argument.
			-look up the formula for calculating the average of numbers.
			-you can save the value of the calculation in a variable.
			-return the result of the average calculation.

	    Create a global variable called outside of the function called averageVar.
			-This variable should be able to receive and store the result of the average calculation
			-Log the value of the averageVar variable in the console.
	

	5. Create a function which will be able to check if you passed by checking the percentage of your score against the passing percentage.
			-this function should take 2 numbers as an argument, your score and the total score.
			-First, get the percentage of your score against the total. You can look up the formula to get percentage.
			-Using a relational operator, check if your score percentage is greater than 75, the passing percentage. Save the value of the comparison in a variable called isPassed.
			-return the value of the variable isPassed.
			-This function should return a boolean.

		Create a global variable called outside of the function called isPassingScore.
			-This variable should be able to receive and store the boolean result of the checker function.
			-Log the value of the isPassingScore variable in the console.
*/

	function printAdd(total) {

		let firstNum = prompt("Enter a number:");
		let secondNum = prompt("Enter another number:");
		let numOne = parseInt(firstNum);
		let numTwo = parseInt(secondNum);
		let sum = numOne + numTwo;
		console.log("Displayed sum of " + firstNum + " and " + secondNum);
		console.log(sum);
		return sum;
	}

	printAdd();

	function printSub(total) {

		let firstNum = prompt("Enter a number:");
		let secondNum = prompt("Enter another number:");
		let numOne = parseInt(firstNum);
		let numTwo = parseInt(secondNum);
		let diff = numOne - numTwo;
		console.log("Displayed difference of " + firstNum + " and " + secondNum);
		console.log(diff);
		return diff;
	}

	printSub();

	function printMul(total) {

		let firstNum = prompt("Enter a number:");
		let secondNum = prompt("Enter another number:");
		let numOne = parseInt(firstNum);
		let numTwo = parseInt(secondNum);
		let product = numOne * numTwo;
		console.log("Displayed product of " + firstNum + " and " + secondNum);
		console.log(product);
		return product;
	}

	printMul();

	function printDiv(total) {

		let firstNum = prompt("Enter a number:");
		let secondNum = prompt("Enter another number:");
		let numOne = parseInt(firstNum);
		let numTwo = parseInt(secondNum);
		let quotient = numOne / numTwo;
		console.log("Displayed quotient of " + firstNum + " and " + secondNum);
		console.log(quotient);
		return quotient;
	}

	printDiv();

	function areaCir(rad) {
		let radius = prompt("Enter a radius:");
		let rad1 = parseInt(radius);
		let	total = rad1 * rad1;
		let totalArea = total * 3.14;
		console.log("The results of getting the area of circle with " + radius + " radius:");
		console.log(totalArea);

	}

	areaCir();


	function aveRage(firstN,secondN,thirdN,fourthN) {

		let	fi = parseInt(firstN);
		let	se = parseInt(secondN);
		let	th = parseInt(thirdN);
		let	fo = parseInt(fourthN);
		let	aveTotal = (fi + se + th + fo) / 4;
		console.log('The average of ' + firstN + ', ' + secondN + ', ' + thirdN + ', and ' + fourthN + ':');
		console.log(aveTotal)
		
	}
		let firstN = prompt("Enter 1st Number");
		let secondN = prompt("Enter 2nd Number");
		let thirdN = prompt("Enter 3rd Number");
		let fourthN = prompt("Enter 4th Number");

		aveRage(firstN,secondN,thirdN,fourthN);


	function reSult(score,totalScore) {

		let sc = parseInt(score);
		let	ts = parseInt(totalScore);
		let finalScore = (sc / ts) * 100;
		console.log('Is ' + sc + '/' + ts + ' passing score?');
		let fScore = finalScore >= 75;
		console.log(fScore)
	}

		let score = prompt("Enter the Score");
		let totalScore = prompt("Enter the Total Score");

		reSult(score,totalScore)